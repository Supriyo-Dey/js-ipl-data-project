const fileUtility = require("./file-utility");

function strikeRate(matchesResult, deliveriesResult) {
    const data = {};

    for (let index = 0; index < matchesResult.length; index++) {
        const season = matchesResult[index].season;
        const matchId = matchesResult[index].id;

        if (data[season]) {
            let matches = data[season];
            data[season] = [...matches, matchId];

        } else {
            data[season] = [matchId];
        }
    }
    
    const year = {};

    for (let match in data) {
        const strikeRatePerYear = {};
        let matchesPerYear = data[match];

        for (let index = 0; index < matchesPerYear.length; index++) {

            let matchId = matchesPerYear[index];
            let matchDetails = deliveriesResult[matchId];
            let batsman = matchDetails.batsman;
            let totalBalls = parseInt(matchDetails.ball);
            let totalRuns = parseInt(matchDetails.total_runs);
            let strikeRate = (totalRuns / totalBalls) * 100;

            if (strikeRatePerYear[batsman]) {

                let previousRuns = strikeRatePerYear[batsman].runs;
                let previousBalls = strikeRatePerYear[batsman].balls;
                let newRuns = parseInt(totalRuns) + parseInt(previousRuns);
                let newBalls = parseInt(totalBalls) + parseInt(previousBalls);

                strikeRate = (newRuns / newBalls) * 100;

                strikeRatePerYear[batsman] = {

                    ...strikeRatePerYear[batsman],
                    runs: newRuns,
                    balls: newBalls,
                    strikeRate: strikeRate

                }
            } else {

                strikeRatePerYear[batsman] = {
                    runs: totalRuns,
                    balls: totalBalls,
                    strikeRate: strikeRate
                };
                
            }


        }
        year[match] = strikeRatePerYear;
    }


    fileUtility.writeToJson("strikeRate",year);
}

fileUtility.getBothData("matches", "deliveries", strikeRate);