const fileUtility = require("./file-utility");

function matchesWonPerTeamPerYear(results) {

    const perYear = {};
    
    for(let index=0; index<results.length; index++){
        if(perYear[results[index].season]){

            const perTeam = {};
            
            for(iter=0; iter<results.length; iter++){

                if(results[iter].season == results[index].season){
                    if(perTeam[results[iter].winner]){
                        perTeam[results[iter].winner] += 1;
                    } else {
                        perTeam[results[iter].winner] = 1;
                    }
                }

            }

            perYear[results[index].season] = perTeam;
        } else {
            perYear[results[index].season] = {};
        }
    }

    fileUtility.writeToJson("matchesWonPerTeamPerYear.json", perYear);
}

fileUtility.getData("matches", matchesWonPerTeamPerYear);