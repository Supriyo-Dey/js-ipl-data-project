const fs = require('fs');
const csv = require('csv-parser');

function writeToJson(filename, data) {
    try {
        let writeStream = fs.createWriteStream(`../public/output/${filename}.json`, 'utf-8');
        writeStream.write(JSON.stringify(data, null, 2));
    } catch {
        console.log("An error occured while writing from the file, kindly recheck");
    }
}


function getData(filename, callback) {
    const results = [];

    fs.createReadStream(`../data/${filename}.csv`)
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', () => {
            try {
                callback(results);                
            } catch {
                console.log("An error occured while using the function, kindly recheck!");
            }
        });
}

function getBothData(filename1,filename2,callback){
    const file1Results = [];
    const file2Results = [];

    fs.createReadStream(`../data/${filename1}.csv`)
        .pipe(csv())
        .on('data', (data) => file1Results.push(data))
        .on('end', () => {
            fs.createReadStream(`../data/${filename2}.csv`)
                .pipe(csv())
                .on('data', (data) => file2Results.push(data))
                .on('end',() => {
                    
                    callback(file1Results,file2Results);
                })
        });
}

exports.getBothData = getBothData;
exports.getData = getData;
exports.writeToJson = writeToJson;