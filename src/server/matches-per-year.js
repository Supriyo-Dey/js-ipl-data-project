const fileUtility = require("./file-utility");

function matchesPerYear(results) {

    const perYear = {};
    
    for(let index=0; index<results.length; index++){
        if(perYear[results[index].season]){
            perYear[results[index].season] += 1;
        } else {
            perYear[[results[index].season]] = 1;
        }
    }

    fileUtility.writeToJson("matchesPerYear", perYear);
}

fileUtility.getData("matches", matchesPerYear);