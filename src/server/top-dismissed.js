const fileUtility = require("./file-utility");

const result = {};
function topDismissed(deliveriesResult){
    
    for(let index=0; index<deliveriesResult.length; index++){

        let batsman = deliveriesResult[index].batsman;
        let bowler = deliveriesResult[index].bowler;
        let dismissal = deliveriesResult[index].dismissal_kind;

        if(dismissal != "run out" && dismissal != ""){

            let combo = batsman + ' - ' + bowler;
            if(result[combo] == undefined){
                result[combo] = 1;
            } else {
                result[combo]++;
            }
        }
    }
    
    let maxCount =0;
    let maxKey = "";

    for(let key in result){
        if(result[key]> maxCount){

            maxCount = result[key];
            maxKey = key;
        }
    }
    
    const finalResult = {};
    finalResult[maxKey] = maxCount;
    fileUtility.writeToJson("topDismissed", finalResult);
    
}

fileUtility.getData("deliveries",topDismissed);
