const fileUtility = require("./file-utility");

function topEconomyBowler(matchesResult, deliveriesResult) {

    let matches = [];

    for (let index = 0; index < matchesResult.length; index++) {
        if (matchesResult[index].season == "2015") {
            matches.push(matchesResult[index].id);
        }
    }

    const bowlers = {};

    for (let index = 0; index < deliveriesResult.length; index++) {

        let delivery = deliveriesResult[index];

        if (matches.includes(delivery.match_id)) {

            let newRuns, newBalls;
            let bowlerName = delivery.bowler;
            let currentRuns = delivery.total_runs;
            let currentBall = delivery.ball;

            if (bowlers[bowlerName]) {

                let previousRuns = bowlers[bowlerName].total_runs;
                let previousBalls = bowlers[bowlerName].total_balls;

                newRuns = parseInt(currentRuns) + parseInt(previousRuns);
                newBalls = parseInt(currentBall) + parseInt(previousBalls);

            } else {

                newRuns = parseInt(currentRuns);
                newBalls = parseInt(currentBall);
            }

            let economy = (newRuns / newBalls) * 6;
            bowlers[bowlerName] = { "total_runs": newRuns, "total_balls": newBalls, "economy": economy };
        }
    }

    let list = [];

    for (let bowler in bowlers) {
        let individualBowler = { name: bowler, economy: bowlers[bowler].economy };
        
        list.push(individualBowler);
    }

    list.sort((bowler1, bowler2) => {
        if (bowler1.economy > bowler2.economy) {
            return 1;
        } else if (bowler1.economy < bowler2.economy) {
            return -1;
        } else {
            return 0;
        }
    });
    
    const result = list.slice(0,10)

    fileUtility.writeToJson("topEconomyBowler", result);
}


fileUtility.getBothData("matches", "deliveries", topEconomyBowler);

