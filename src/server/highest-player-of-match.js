const fileUtility = require("./file-utility");

function highestPlayerOfMatch(result) {

    const output = {};
    
    for(let index=0;index<result.length; index++){

        if(!output[result[index].season]){
            output[result[index].season] = {};
        }

        let season = output[result[index].season];
        let player = result[index].player_of_match;

        if(season[player]){
            season[player] += 1;
        } else {
            season[player] = 1;
        }
    }

    for(let index in output){

        let season = output[index];
        let maxPlayer;
        let count=0;

        for(let iter in season){
            if(season[iter]>count){
                count = season[iter];
                maxPlayer = iter;
            }
        }
        
        output[index] = maxPlayer;
    }
    fileUtility.writeToJson("highestPlayerOfMatch",output);
}

fileUtility.getData("matches", highestPlayerOfMatch);
