const fileUtility = require("./file-utility");

function wonTossWonMatch(matchResult){
    const result = {};

    for(let index=0; index<matchResult.length; index++){
        if(result[matchResult[index].winner]){
            if(matchResult[index].winner == matchResult[index].toss_winner){

                result[matchResult[index].winner] += 1;
            }
        } else {

            result[matchResult[index].winner] = 1;
        }
    }
    
    fileUtility.writeToJson("wonTossWonMatch",result);
}

fileUtility.getData("matches",wonTossWonMatch);