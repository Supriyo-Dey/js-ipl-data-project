const fileUtility = require("./file-utility");

function extrasPerTeam(matchesResults,deliveriesResult){

    const result = {};

    for(let index=0; index<matchesResults.length; index++){
        for(let iter=0; iter<deliveriesResult.length; iter++){
            if(deliveriesResult[iter].match_id  == matchesResults[index].id && matchesResults[index].season == '2016'){

                if(result[deliveriesResult[iter].bowling_team]){
                    result[deliveriesResult[iter].bowling_team] = result[deliveriesResult[iter].bowling_team] + parseInt(deliveriesResult[iter].extra_runs);
                } else {
                    result[deliveriesResult[iter].bowling_team] = 1;
                }
                
            }
        }
    }

    fileUtility.writeToJson("extrasPerTeam", result);
}

fileUtility.getBothData("matches","deliveries",extrasPerTeam);